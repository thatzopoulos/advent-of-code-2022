use itertools::Itertools;
use log::info;

// 74711
pub fn part1() {
    let result = include_str!("../input/day01.txt")
        .split("\n\n")
        .map(|x| {
            x.lines()
                .map(|x| x.parse::<usize>().unwrap())
                .sum::<usize>()
        })
        .sorted()
        .max();

    info!("Part 1: {:#?}", result);
}

// 209481
pub fn part2() {
    let result = include_str!("../input/day01.txt")
        .split("\n\n")
        .map(|x| {
            x.lines()
                .map(|x| x.parse::<usize>().unwrap())
                .sum::<usize>()
        })
        .sorted()
        .rev()
        .take(3)
        .sum::<usize>();
    info!("Part 2: {:#?}", result);
}
