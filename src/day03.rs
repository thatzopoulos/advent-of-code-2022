use itertools::Itertools;
use log::info;
use std::collections::HashSet;

fn calc_score(character: char) -> i64 {
    match character {
        'a' => 1,
        'b' => 2,
        'c' => 3,
        'd' => 4,
        'e' => 5,
        'f' => 6,
        'g' => 7,
        'h' => 8,
        'i' => 9,
        'j' => 10,
        'k' => 11,
        'l' => 12,
        'm' => 13,
        'n' => 14,
        'o' => 15,
        'p' => 16,
        'q' => 17,
        'r' => 18,
        's' => 19,
        't' => 20,
        'u' => 21,
        'v' => 22,
        'w' => 23,
        'x' => 24,
        'y' => 25,
        'z' => 26,
        'A' => 27,
        'B' => 28,
        'C' => 29,
        'D' => 30,
        'E' => 31,
        'F' => 32,
        'G' => 33,
        'H' => 34,
        'I' => 35,
        'J' => 36,
        'K' => 37,
        'L' => 38,
        'M' => 39,
        'N' => 40,
        'O' => 41,
        'P' => 42,
        'Q' => 43,
        'R' => 44,
        'S' => 45,
        'T' => 46,
        'U' => 47,
        'V' => 48,
        'W' => 49,
        'X' => 50,
        'Y' => 51,
        'Z' => 52,
        _ => unreachable!(),
    }
}

//7716
pub fn part1() {
    let mut result = 0;
    for line in include_str!("../input/day03.txt").lines() {
        let line = line.chars().collect::<Vec<_>>();
        let comp1 = line.as_slice()[..line.len() / 2]
            .iter()
            .collect::<HashSet<_>>();
        let comp2 = line.as_slice()[line.len() / 2..]
            .iter()
            .collect::<HashSet<_>>();
        let common = **(comp1.intersection(&comp2)).next().unwrap() as char;
        result += calc_score(common as char)
    }
    info!("Part 1: {:#?}", result);
}

//2973
pub fn part2() {
    let mut result = 0;
    for (rucksack1, rucksack2, rucksack3) in include_str!("../input/day03.txt").lines().tuples() {
        let hash1 = rucksack1.chars().collect::<HashSet<_>>();
        let hash2 = rucksack2.chars().collect::<HashSet<_>>();
        let hash3 = rucksack3.chars().collect::<HashSet<_>>();
        let common = *(hash1
            .intersection(&hash2)
            .copied()
            .collect::<HashSet<char>>()
            .intersection(&hash3))
        .next()
        .unwrap() as char;
        result += calc_score(common)
    }
    info!("Part 2: {:#?}", result);
}
