use std::collections::HashSet;

use itertools::Itertools;
use log::{debug, info, warn};

// 1640
pub fn part1() {
    let input = include_str!("../input/day06.txt");
    let mut location = 0;
    for (a, b, c, d) in input.chars().tuple_windows() {
        // Note from the future: tuple_windows can only handle up to 12 elements which is why I did not use it in part 2
        if vec![a, b, c, d]
            .into_iter()
            .tuple_combinations()
            .all(|(x, y)| x != y)
        {
            debug!("No Duplicates : {} {} {} {} at ", a, b, c, d);
            break;
        } else {
            location += 1
        }
    }
    let result = location + 4; // location + 4 char marker
    info!("Part 1: {:#?}", result);
}

// 3613
pub fn part2() {
    let input = include_str!("../input/day06.txt")
        .chars()
        .collect::<Vec<char>>();
    let search = input
        .as_slice()
        .windows(14)
        .enumerate()
        .find(|(_, y)| y.iter().collect::<HashSet<_>>().len() == y.len())
        .unwrap();
    debug!("search results: {:#?}", search);
    let result = search.0 as usize + 14; // location + 14 char marker
    info!("Part 2: {:#?}", result);
}
