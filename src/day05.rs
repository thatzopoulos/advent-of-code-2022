use itertools::Itertools;
use log::{debug, info};

#[derive(Debug)]
pub struct Harbor {
    pub stacks: Vec<Vec<char>>,
    pub multi_pick: bool,
}

#[derive(Debug)]
pub struct Instruction {
    pub from: usize,
    pub to: usize,
    pub amount: usize,
}

const STACK_AMOUNT: usize = 9;

// VJSFHWGFT
pub fn part1() {
    let instructions: Vec<_> = include_str!("../input/day05.txt").split("\n\n").collect();
    debug!("{:#?}", instructions);
    let state = instructions[0];

    let mut harbor = create_harbor_state(state, false);
    let instructions = parse_instructions(&instructions);
    for instruction in instructions {
        debug!("Instruction: {:#?}", instruction);
        harbor.execute_instruction(&instruction);
    }
    debug!("Harbor: {:#?}", harbor);
    let result = harbor.get_top_row();
    info!("Part 1: {:#?}", result);
}

// LCTQFBVZV
pub fn part2() {
    let instructions: Vec<_> = include_str!("../input/day05.txt").split("\n\n").collect();
    debug!("{:#?}", instructions);
    let state = instructions[0];

    let mut harbor = create_harbor_state(state, true);
    let instructions = parse_instructions(&instructions);
    for instruction in instructions {
        debug!("Instruction: {:#?}", instruction);
        harbor.execute_instruction(&instruction);
    }
    debug!("Harbor: {:#?}", harbor);
    let result = harbor.get_top_row();
    info!("Part 2: {:#?}", result);
}

// Helper Functions

pub fn create_harbor_state(state: &str, crate_mover_9001: bool) -> Harbor {
    let lines: Vec<&str> = state.lines().rev().skip(1).collect();
    debug!("Initial Harbor State: {:#?}", lines);

    let stack_capacity = lines.len() * 2; // Lazily doubling it, should be enough stack capacity

    let mut harbor = Harbor::new(stack_capacity, crate_mover_9001);
    for line in lines {
        let chars: Vec<char> = line.chars().collect();
        for stack in 0..STACK_AMOUNT {
            let character = chars[1 + (stack * 4)];

            // debug!("Chars: {:#?}", character);
            if !character.is_whitespace() {
                harbor.add_crate(stack, character);
            }
        }
    }

    debug!("Final Harbor State: {:#?}", harbor);
    harbor
}
fn parse_instructions<'a>(instructions: &'a Vec<&str>) -> impl Iterator<Item = Instruction> + 'a {
    instructions[1]
        .split('\n')
        .filter(|x| x != &"")
        .map(|parts| {
            debug!("Parts: {:#?}", parts);
            let (amount, from, to) =
                scan_fmt!(parts, "move {d} from {d} to {d}", usize, usize, usize).unwrap();
            Instruction {
                from: from - 1,
                to: to - 1,
                amount,
            }
        })
}

impl Harbor {
    pub fn new(stack_capacity: usize, multi_pick: bool) -> Self {
        let mut stacks = Vec::with_capacity(STACK_AMOUNT);
        for _ in 0..STACK_AMOUNT {
            stacks.push(Vec::with_capacity(stack_capacity));
        }
        Harbor { stacks, multi_pick }
    }

    pub fn add_crate(&mut self, stack: usize, character: char) {
        self.stacks[stack].push(character)
    }

    pub fn execute_instruction(&mut self, instruction: &Instruction) {
        let from_stack = &mut self.stacks[instruction.from];
        let mut crates = from_stack
            .drain((from_stack.len() - instruction.amount)..from_stack.len())
            .collect::<Vec<char>>();

        if !self.multi_pick {
            crates.reverse();
        }
        self.stacks[instruction.to].extend(crates);
    }
    pub fn get_top_row(&self) -> String {
        let mut row = String::with_capacity(self.stacks.capacity());
        for stack in &self.stacks {
            row.push(*stack.last().unwrap());
        }
        row
    }
}
