use itertools::Itertools;
use log::info;

// A = Rock
// B = Paper
// C = Scissors
// X = Rock
// Y = Paper
// Z = Scissors

//10816
pub fn part1() {
    let result: i64 = include_str!("../input/day02.txt")
        .lines()
        .map(|x| {
            let (p1, p2) = x
                .split_whitespace()
                .collect_tuple::<(&str, &str)>()
                .unwrap();
            match p1 {
                "A" => match p2 {
                    "X" => 4,
                    "Y" => 8,
                    "Z" => 3,
                    _ => panic!(),
                },
                "B" => match p2 {
                    "X" => 1,
                    "Y" => 5,
                    "Z" => 9,
                    _ => panic!(),
                },
                "C" => match p2 {
                    "X" => 7,
                    "Y" => 2,
                    "Z" => 6,
                    _ => panic!(),
                },
                _ => panic!(),
            }
        })
        .sum();

    info!("Part 1: {:#?}", result);
}

// 209481
pub fn part2() {
    let result: i64 = include_str!("../input/day02.txt")
        .lines()
        .map(|x| {
            let (p1, p2) = x
                .split_whitespace()
                .collect_tuple::<(&str, &str)>()
                .unwrap();
            match p1 {
                "A" => match p2 {
                    // if opponent chooses rock
                    "X" => 3, // end in loss, choose scissors, 3 points
                    "Y" => 4, // end in draw, choose rock, 4 points
                    "Z" => 8, // end in win, choose paper, 8 points
                    _ => panic!(),
                },
                "B" => match p2 {
                    "X" => 1,
                    "Y" => 5,
                    "Z" => 9,
                    _ => panic!(),
                },
                "C" => match p2 {
                    "X" => 2,
                    "Y" => 6,
                    "Z" => 7,
                    _ => panic!(),
                },
                _ => panic!(),
            }
        })
        .sum();

    info!("Part 2: {:#?}", result);
}
