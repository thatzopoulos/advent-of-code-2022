mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;

use colored::Colorize;
use env_logger::{Env, Target};
use rayon::prelude::*;
use took::Timer;
#[macro_use]
extern crate scan_fmt;

const NUMBER_OF_DASHES: usize = 80;

fn header() {
    println!("{}", "-".repeat(NUMBER_OF_DASHES).green().bold());
    println!(
        "{} {} {}",
        "-".repeat(NUMBER_OF_DASHES / 2 - 10).red().bold(),
        "Advent Of Code 2022".bold(),
        "-".repeat(NUMBER_OF_DASHES / 2 - 11).red().bold()
    );
    println!("{}", "-".repeat(NUMBER_OF_DASHES).green().bold());

    let tree_star = r#"           *             ,
                       _/^\_
                      <     >
     *                 /.-.\         *"#;
    let tree_art = r#"              *        `/&\`                   *
                      ,@.*;@,
                     /_o.I %_\    *
        *           (`'--:o(_@;
                   /`;--.,__ `')             *
                  ;@`o % O,*`'`&\
            *    (`'--)_@ ;o %'()\      *
                 /`;--._`''--._O'@;
                /&*,()~o`;-.,_ `""`)
     *          /`,@ ;+& () o*`;-';\
               (`""--.,_0 +% @' &()\
               /-.,_    ``''--....-'`)  *
          *    /@%;o`:;'--,.__   __.'\
              ;*,&(); @ % &^;~`"`o;@();         *
              /(); o^~; & ().o@*&`;&%O\
              `"="==""==,,,.,="=="==="`"#;
    let tree_bottom = r#"           __.----.(\-''#####---...___...-----._
         '`         \)_`"""""`
                 .--' ')
               o(  )_-\
                 `"""` `
                 "#;
    println!(
        "{}\n{}\n{}",
        tree_star.bright_yellow(),
        tree_art.bright_green(),
        tree_bottom.bright_black()
    );
}

fn main() {
    let env = Env::default()
        .filter_or("RUST_LOG", "info")
        // Normally using a pipe as a target would mean a value of false, but this forces it to be true.
        .write_style_or("RUST_LOG_STYLE", "always");

    env_logger::Builder::from_env(env)
        .target(Target::Stdout)
        .init();

    header();
    runner();
}

// Runner Code
fn runner() {
    rayon::ThreadPoolBuilder::new()
        .num_threads(4)
        .stack_size(12_800_000)
        .build_global()
        .unwrap();

    let jobs = jobs_queue();
    let timer = Timer::new();
    (0..jobs.len()).into_par_iter().for_each(|i| {
        // timer.took().describe(jobs[i].1);
        jobs[i].0()
    });
    timer.took().describe("Running all days");
}

fn jobs_queue() -> &'static [(fn(), &'static str)] {
    &[
        // (day01::part1, "Day1 Pt1"),
        // (day01::part2, "Day1 Pt2"),
        // (day02::part1, "Day2 Pt1"),
        // (day02::part2, "Day2 Pt2"),
        // (day03::part1, "Day3 Pt1"),
        // (day03::part2, "Day3 Pt2"),
        // (day04::part1, "day04 Pt1"),
        // (day04::part2, "day04 Pt2"),
        // (day05::part1, "day05 Pt1"),
        // (day05::part2, "day05 Pt2"),
        // (day06::part1, "day06 Pt1"),
        // (day06::part2, "day06 Pt2"),
        (day07::part1, "day07 Pt1"),
        (day07::part2, "day07 Pt2"),
        // (day08::part1, "day08 Pt1"),
        // (day08::part1, "day08 Pt1"),
        // (day09::part1, "day09 Pt1"),
        // (day09::part1, "day09 Pt1"),
        // (day10::part1, "day10 Pt1"),
        // (day10::part1, "day10 Pt1"),
        // (day11::part1, "day11 Pt1"),
        // (day11::part1, "day11 Pt1"),
        // (day12::part1, "day12 Pt1"),
        // (day12::part1, "day12 Pt1"),
        // (day13::part1, "day13 Pt1"),
        // (day13::part1, "day13 Pt1"),
        // (day14::part1, "day14 Pt1"),
        // (day14::part1, "day14 Pt1"),
        // (day15::part1, "day15 Pt1"),
        // (day15::part1, "day15 Pt1"),
        // (day16::part1, "day16 Pt1"),
        // (day16::part1, "day16 Pt1"),
        // (day17::part1, "day17 Pt1"),
        // (day17::part1, "day17 Pt1"),
        // (day18::part1, "day18 Pt1"),
        // (day18::part1, "day18 Pt1"),
        // (day19::part1, "day19 Pt1"),
        // (day19::part1, "day19 Pt1"),
        // (day20::part1, "day20 Pt1"),
        // (day20::part1, "day20 Pt1"),
        // (day21::part1, "day21 Pt1"),
        // (day21::part1, "day21 Pt1"),
        // (day22::part1, "day22 Pt1"),
        // (day22::part1, "day22 Pt1"),
        // (day23::part1, "day23 Pt1"),
        // (day23::part1, "day23 Pt1"),
        // (day24::part1, "day24 Pt1"),
        // (day24::part1, "day24 Pt1"),
        // (day25::part1, "day25 Pt1"),
    ]
}
