use log::{info, warn};

pub fn part1() {
    let result = include_str!("../input/day04.txt")
        .lines()
        .filter_map(|l| {
            let (x, y) = l.split_once(',').unwrap();
            let (a, b) = x.split_once('-').unwrap();
            let (c, d) = y.split_once('-').unwrap();
            Some((
                a.parse::<usize>().unwrap(),
                b.parse::<usize>().unwrap(),
                c.parse::<usize>().unwrap(),
                d.parse::<usize>().unwrap(),
            ))
        })
        .filter(|(x1, y1, x2, y2)| (x1 <= x2 && y1 >= y2) || (x2 <= x1 && y2 >= y1))
        .count();
    info!("Part 1: {:#?}", result);
}

pub fn part2() {
    let result = include_str!("../input/day04.txt")
        .lines()
        .filter_map(|l| {
            let (x, y) = l.split_once(',').unwrap();
            let (a, b) = x.split_once('-').unwrap();
            let (c, d) = y.split_once('-').unwrap();
            Some((
                a.parse::<usize>().unwrap(),
                b.parse::<usize>().unwrap(),
                c.parse::<usize>().unwrap(),
                d.parse::<usize>().unwrap(),
            ))
        })
        .filter(|(x1, y1, x2, y2)| x1 <= y2 && x2 <= y1)
        .count();
    info!("Part 2: {:#?}", result);
}
