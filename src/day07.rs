use std::collections::HashMap;

use itertools::Itertools;
use log::{debug, info, warn};

// 1845346
pub fn part1() {
    let max_size = 100000;
    let result: usize = parse_cmd_history(include_str!("../input/day07.txt"))
        .iter()
        .filter(|&x| x <= &max_size)
        .sum();

    info!("Part 1: {:#?}", result);
}

// 3633703
pub fn part2() {
    let max_size = 70000000;
    let dirs = parse_cmd_history(include_str!("../input/day07.txt"));
    let largest_dir = dirs.iter().max().unwrap();
    let unused_size = max_size - largest_dir;
    let required_size = 30000000;
    let size_to_free_up = required_size - unused_size;

    let result = dirs
        .iter()
        .filter(|&x| x >= &size_to_free_up)
        .min()
        .unwrap();
    info!("Part 2: {:#?}", result);
}

pub fn parse_cmd_history(input: &str) -> Vec<usize> {
    let mut cwd: Vec<String> = Vec::new();
    let mut dirs: HashMap<String, Vec<usize>> = HashMap::new();

    for line in input.lines() {
        if line == "$ cd .." {
            cwd.pop();
        } else if line.starts_with("$ cd ") {
            cwd.push(line.split_once("$ cd ").unwrap().1.parse().unwrap());
            dirs.insert(format!("/{}", cwd.iter().skip(1).join("/")), Vec::new());
        } else if line.starts_with(|x: char| x.is_numeric()) {
            let size = line.split_once(" ").unwrap().0.parse().unwrap();
            let files = dirs
                .get_mut(&format!("/{}", cwd.iter().skip(1).join("/")))
                .unwrap();
            files.push(size);
        }
    }

    // calculate sums
    let mut result = vec![0; dirs.keys().len()]; // use dir size to create vec
    for (i, dir) in dirs.keys().enumerate() {
        for subdir in dirs.keys().filter(|x| x.starts_with(dir)) {
            let subdir_contains = dirs.get(subdir).unwrap();
            result[i] += subdir_contains.iter().sum::<usize>()
        }
    }
    result
}
