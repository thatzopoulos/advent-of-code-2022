use itertools::Itertools;
use log::{debug, info, warn};

//
pub fn part1() {
    let result = include_str!("../input/day00.txt").split("\n\n");

    info!("Part 1: {:#?}", result);
}

//
pub fn part2() {
    let result = include_str!("../input/day00.txt").split("\n\n");
    info!("Part 2: {:#?}", result);
}
